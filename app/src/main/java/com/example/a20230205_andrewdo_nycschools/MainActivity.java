package com.example.a20230205_andrewdo_nycschools;

import static com.example.a20230205_andrewdo_nycschools.Const.SCHOOL_DIRECTORY;
import static com.example.a20230205_andrewdo_nycschools.Const.SCHOOL_SCORES;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.a20230205_andrewdo_nycschools.databinding.ActivityMainBinding;
import com.example.a20230205_andrewdo_nycschools.model.DirectoryPOJO;
import com.example.a20230205_andrewdo_nycschools.model.ScoresPOJO;
import com.example.a20230205_andrewdo_nycschools.request.Service;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
{
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        Service.execute(this, binding);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }


}

/**
 * Overview
 *
 * 1. Use retrofit to make api requests and store model data into hashmaps <String, Model>.
 *   These 2 data sets will be linked by the DBN key which refers to the school.
 *  - Request to school list dataset
 *  - Request to sat scores dataset
 * 1b. show loading animation while waiting for data to load
 * 2. Display school information as cards in a recycler view
 *  - cards have expand/collapse behavior, and will show SAT data if it exists.
 */