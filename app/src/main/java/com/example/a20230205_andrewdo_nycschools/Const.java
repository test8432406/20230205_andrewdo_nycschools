package com.example.a20230205_andrewdo_nycschools;

import com.example.a20230205_andrewdo_nycschools.model.DirectoryPOJO;
import com.example.a20230205_andrewdo_nycschools.model.ScoresPOJO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//Use const class to keep string/constant references centralized
public class Const
{
    public static List<DirectoryPOJO> SCHOOL_DIRECTORY;
    public static final HashMap<String, ScoresPOJO> SCHOOL_SCORES = new HashMap<>();

    public static final String URL_BASE = "https://data.cityofnewyork.us";
    public static final String URL_SCHOOL_DIRECTORY = "/resource/s3k6-pzi2.json";
    public static final String URL_SCHOOL_SCORES = "/resource/f9bf-2cp4.json";
}
