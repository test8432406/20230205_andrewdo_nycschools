package com.example.a20230205_andrewdo_nycschools;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.example.a20230205_andrewdo_nycschools.databinding.ActivitySchoolDirectoryBinding;
import com.example.a20230205_andrewdo_nycschools.model.DirectoryPOJO;
import com.example.a20230205_andrewdo_nycschools.viewmodel.DirectoryAdapter;
import com.example.a20230205_andrewdo_nycschools.viewmodel.DirectoryViewModel;

import java.util.List;

public class SchoolDirectoryActivity extends AppCompatActivity {

    private ActivitySchoolDirectoryBinding binding;
    private DirectoryViewModel directoryViewModel;

    DirectoryAdapter directoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySchoolDirectoryBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        directoryViewModel = new ViewModelProvider(this).get(DirectoryViewModel.class);
        directoryViewModel.getSchoolDirectory().observe(this, directoryUpdateObserver);

        directoryAdapter = new DirectoryAdapter();
        binding.recyclerview.setAdapter(directoryAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerview.setLayoutManager(layoutManager);
        int scrollPosition = 0;
        if (binding.recyclerview.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) binding.recyclerview.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        binding.recyclerview.scrollToPosition(scrollPosition);



    }

    Observer<List<DirectoryPOJO>> directoryUpdateObserver = new Observer<List<DirectoryPOJO>>() {
        @Override
        public void onChanged(List<DirectoryPOJO> directory) {
            directoryAdapter.updateDirectory(directory);
        }
    };

}