package com.example.a20230205_andrewdo_nycschools.request;

import static com.example.a20230205_andrewdo_nycschools.Const.URL_SCHOOL_DIRECTORY;
import static com.example.a20230205_andrewdo_nycschools.Const.URL_SCHOOL_SCORES;

import com.example.a20230205_andrewdo_nycschools.model.DirectoryPOJO;
import com.example.a20230205_andrewdo_nycschools.model.ScoresPOJO;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SchoolApi
{
    @GET(URL_SCHOOL_DIRECTORY)
    Call<List<DirectoryPOJO>> getSchoolDirectory();

    @GET(URL_SCHOOL_SCORES)
    Call<List<ScoresPOJO>> getSchoolScores();

}
