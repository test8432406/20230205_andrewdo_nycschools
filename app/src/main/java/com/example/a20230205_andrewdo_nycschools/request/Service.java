package com.example.a20230205_andrewdo_nycschools.request;

import static com.example.a20230205_andrewdo_nycschools.Const.SCHOOL_DIRECTORY;
import static com.example.a20230205_andrewdo_nycschools.Const.SCHOOL_SCORES;
import static com.example.a20230205_andrewdo_nycschools.Const.URL_BASE;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import com.example.a20230205_andrewdo_nycschools.MainActivity;
import com.example.a20230205_andrewdo_nycschools.SchoolDirectoryActivity;
import com.example.a20230205_andrewdo_nycschools.databinding.ActivityMainBinding;
import com.example.a20230205_andrewdo_nycschools.model.DirectoryPOJO;
import com.example.a20230205_andrewdo_nycschools.model.ScoresPOJO;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Service
{
    private static final Retrofit retrofit =
            new Retrofit.Builder()
                    .baseUrl(URL_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

    private static final SchoolApi schoolApi = retrofit.create(SchoolApi.class);

    public static SchoolApi getSchoolApi() {
        return schoolApi;
    }

    public static void execute(AppCompatActivity activity, ActivityMainBinding binding)
    {
        Service.activity = activity;
        new DisplaySplash().execute();
        TextView loadingText = binding.mainProgressText;
        ProgressBar progressBar = binding.mainProgressbar;

        Call<List<DirectoryPOJO>> directoryCall = Service.getSchoolApi().getSchoolDirectory();
        Call<List<ScoresPOJO>> scoresCall = Service.getSchoolApi().getSchoolScores();

        loadingText.setText("Loading Data...");

        directoryCall.enqueue(new Callback<List<DirectoryPOJO>>() {
            @Override
            public void onResponse(Call<List<DirectoryPOJO>> call, Response<List<DirectoryPOJO>> response) {
                if (response.code() != 200) return; //TODO: set the loading screen text to error
                if (response.body() == null)
                {
                    return;
                }

                SCHOOL_DIRECTORY = response.body();
                totalProgress += 50.0F;
                progressBar.setProgress((int) totalProgress);
                loadedDirectory = true;
            }

            @Override
            public void onFailure(Call<List<DirectoryPOJO>> call, Throwable t) {

            }
        });

        scoresCall.enqueue(new Callback<List<ScoresPOJO>>()
        {
            @Override
            public void onResponse(Call<List<ScoresPOJO>> call, Response<List<ScoresPOJO>> response)
            {
                if (response.code() != 200) return;
                List<ScoresPOJO> lst = response.body();

                float progressIncrement = 1.0F / lst.size() * 50.0F;

                for (int i = 0; i < lst.size(); i++)
                {
                    SCHOOL_SCORES.put(lst.get(i).dbn, lst.get(i));
                    totalProgress += progressIncrement;
                    progressBar.setProgress((int) totalProgress);
                }
                loadedScores = true;

                //progressBar.setProgress(100);
            }

            @Override
            public void onFailure(Call<List<ScoresPOJO>> call, Throwable t)
            {

            }
        });

    }



    public static class DisplaySplash extends AsyncTask
    {
        @Override
        protected Object doInBackground(Object[] objects)
        {
            while(!loadedScores || !loadedDirectory)
            {
                //wait
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o)
        {
            Service.activity.startActivity(new Intent(Service.activity, SchoolDirectoryActivity.class));
        }
    }

    public static AppCompatActivity activity;
    public static float totalProgress = 0F;
    public static boolean loadedScores = false;
    public static boolean loadedDirectory = false;


}
