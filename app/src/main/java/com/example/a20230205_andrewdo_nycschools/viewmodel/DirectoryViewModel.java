package com.example.a20230205_andrewdo_nycschools.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.a20230205_andrewdo_nycschools.Const;
import com.example.a20230205_andrewdo_nycschools.model.DirectoryPOJO;
import com.example.a20230205_andrewdo_nycschools.model.ScoresPOJO;

import java.util.HashMap;
import java.util.List;

public class DirectoryViewModel extends ViewModel
{
    private MutableLiveData<List<DirectoryPOJO>> schoolDirectory;
    private MutableLiveData<HashMap<String, ScoresPOJO>> schoolScores;

    public DirectoryViewModel() {
        schoolDirectory = new MutableLiveData<>( Const.SCHOOL_DIRECTORY );
        schoolScores = new MutableLiveData<>( Const.SCHOOL_SCORES );
    }


    public LiveData<List<DirectoryPOJO>> getSchoolDirectory()
    {
        return schoolDirectory;
    }

    public LiveData<HashMap<String, ScoresPOJO>> getSchoolScores()
    {
        return schoolScores;
    }


}
