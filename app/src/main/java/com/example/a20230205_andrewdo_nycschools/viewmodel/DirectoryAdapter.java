package com.example.a20230205_andrewdo_nycschools.viewmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20230205_andrewdo_nycschools.Const;
import com.example.a20230205_andrewdo_nycschools.databinding.CardBinding;
import com.example.a20230205_andrewdo_nycschools.model.DirectoryPOJO;
import com.example.a20230205_andrewdo_nycschools.model.ScoresPOJO;

import java.util.List;

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.ViewHolder>
{
    CardBinding binding;
    List<DirectoryPOJO> schools;


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        binding = CardBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        DirectoryPOJO school = schools.get(position);
        holder.name.setText(school.schoolName);
        holder.address.setText(school.location.replaceAll("\\([^()]*\\)", ""));
        holder.description.setText(school.overviewParagraph);

        if (Const.SCHOOL_SCORES.containsKey(school.dbn))
        {
            ScoresPOJO scores = Const.SCHOOL_SCORES.get(school.dbn);
            holder.math.setText("Math: " + scores.satMathAvgScore);
            holder.reading.setText("Reading: " + scores.satCriticalReadingAvgScore);
            holder.writing.setText("Writing: " + scores.satWritingAvgScore);

        }
        else
        {
            holder.title.setText("NO SAT SCORES");
        }

    }

    @Override
    public int getItemCount()
    {
        return schools.size();
    }

    public void updateDirectory(final List<DirectoryPOJO> directory) {
        if (this.schools != null) this.schools.clear();
        this.schools = directory;
        notifyDataSetChanged();
    }

    //ViewHolder class
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView name, address, description, math, reading, writing, title;

        public ViewHolder(CardBinding binding) {
            super(binding.getRoot());

            name = binding.cardText1;
            address = binding.cardText2;
            description = binding.cardText4;
            title = binding.cardText5;
            math = binding.cardText6;
            reading = binding.cardText7;
            writing = binding.cardText8;

        }
    }
}
